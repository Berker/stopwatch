import 'dart:math';

import 'package:flutter/material.dart';
import 'package:stopwatch/ui/clock_markers.dart';

import 'clock_hand.dart';
import 'elapsed_time_text.dart';

class StopwatchRenderer extends StatelessWidget {
  const StopwatchRenderer({
    Key? key,
    required this.elapsed,
    required this.radius,
  }) : super(key: key);
  final Duration elapsed;
  final double radius;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        //Easy way to draw the circle:
        /* Container(
             decoration: BoxDecoration(
             border: Border.all(width: 3, color: Colors.orange),
             shape: BoxShape.circle,
           ),                                // But we will need the radius to draw other stuff so we calculate it in stopwatch.dart and pass it here...
          ),    */
        /*Container(
          decoration: BoxDecoration(
            border: Border.all(width: 3, color: Colors.orange),
            borderRadius: BorderRadius.circular(radius),
          ),
        ),*/
        for (var i = 0; i < 60; i++)
          Positioned(
            left: radius,
            top: radius,
            child: ClockSecondsTickMarker(
              seconds: i,
              radius: radius,
            ),
          ),
        for (var i = 5; i <= 60; i += 5)
          Positioned(
            top: radius,
            left: radius,
            child: ClockTextMarker(
              value: i,
              maxValue: 60,
              radius: radius,
            ),
          ),
        Positioned(
          left: radius,
          top: radius,
          child: ClockHand(
            handLength: radius,
            handThickness: 2,
            rotationZAngle: pi + (2 * pi / 60000) * elapsed.inMilliseconds,
            color: Colors.orange,
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          top: radius * 1.3,
          child: ElapsedTimeText(
            elapsed: elapsed,
          ),
        ),
      ],
    );
  }
}
